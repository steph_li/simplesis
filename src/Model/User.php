<?php

namespace SimpleSIS\Model;

use SimpleSIS\Provider\DB;

/**
 * A user of the SIS
 */
class User {
    /** @var int User ID */
    protected $user_id = 0;
    /** @var string Username */
    protected $username;
    /** @var string Users display name */
    protected $display_name;

    /**
     * Constructor
     *
     * @param array $params
     */
    public function __construct($params = array()) {
        if (isset($params['user_id'])) {
            $this->setUserId($params['user_id']);
        }
        if (isset($params['username'])) {
            $this->setUsername($params['username']);
        }
        if (isset($params['display_name'])) {
            $this->setDisplayName($params['display_name']);
        }
    }

    /**
     * Base database query
     *
     * @return void
     */
    protected static function query() {
        return "SELECT user_id, username, display_name FROM users";
    }

    /**
     * Finds a user in the storage based off the id
     *
     * @param integer $user_id
     * @param DB $db The database connector
     * @return SimpleSis\Model\User
     */
    public static function findById(int $user_id, DB $db) {
        $record = $db->rowQuery(static::query() . "WHERE user_id = :user_id", [
            'user_id' => $user_id
        ]);

        if (!$record) {
            throw new \Exception("Object Not Found");
        }

        return new self($record);
    }

    /**
     * Finds a user in the storage based off the username
     *
     * @param string $user_id
     * @param DB $db The database connector
     * @return SimpleSis\Model\User
     */
    public static function findByUsername($username, DB $db) {
        $record = $db->rowQuery(static::query() . " WHERE username = :username", [
            'username' => $username
        ]);

        if (!$record) {
            throw new \Exception("Object Not Found");
        }

        return new self($record);
    }

    /**
     * Looks up all users in storage
     *
     * @param DB $db The database connector
     * @return SimpleSis\Model\User[]
     */
    public static function lookupAll(DB $db) {
        $users = array();

        $records = $db->Query(static::query());

        foreach ($records as $record) {
            $users[] = new self($record);
        }

        return $users;
    }

    /**
     * Verfies that a given password for a user matches the hash in storage
     *
     * @param string $username
     * @param string $password
     * @param DB $db The database connector
     * @return boolean
     */
    public static function verifyPasswordByUsername(string $username, string $password, DB $db) {
        $password_hash = $db->fieldQuery("SELECT password FROM users where username = :username", [
            'username'  => $username
        ]);

        if (!$password_hash) {
            return false;
        } else {
            if ($password == $password_hash) {
                // Password isn't hashed (it was directly inserted), rehash it below
                $verify = true;
            } else {
                $verify = password_verify($password, $password_hash);
            }

            if ($verify && password_needs_rehash($password_hash, PASSWORD_DEFAULT)) {
                // Update the password hash for this user
                $user = self::findByUsername($username, $db);
                $user->savePassword($password, $db);
            }

            return $verify;
        }
    }

    /**
     * Returns the users id
     *
     * @return int
     */
    public function getUserId() {
        return $this->user_id;
    }
    /**
     * Returns the users username
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }
    /**
     * Returns the users display name
     *
     * @return string
     */
    public function getDisplayName() {
        return $this->display_name;
    }

    /**
     * Sets the users id
     *
     * @param int $user_id
     * @return User
     */
    public function setUserId(int $user_id) {
        $this->user_id = $user_id;
        return $this;
    }
    /**
     * Sets the users username
     *
     * @param int $user_id
     * @return User
     */
    public function setUsername(string $username)  {
        $this->username = $username;
        return $this;
    }
    /**
     * Sets the users display name
     *
     * @param int $user_id
     * @return User
     */
    public function setDisplayName(string $display_name) {
        $this->display_name = $display_name;
        return $this;
    }

    /**
     * Saves the model to storage
     *
     * @param DB $db
     * @return User
     */
    public function save(DB $db) {
        $params = [
            'username'      => $this->getUsername(),
            'display_name'  => $this->getDisplayName(),
        ];

        if ($this->getUserId() == 0) {
            // New Model
            $id = $db->insert("
                INSERT INTO users (
                    username,
                    display_name
                ) VALUES (
                    :username,
                    :display_name
                )",
                $params);

            $this->setUserId($id);
        } else {
            $params['user_id'] = $this->getUserId();

            $db->exec("
                UPDATE users SET
                    username        = :username,
                    display_name    = :display_name
                WHERE user_id = :user_id",
                $params);
        }

        return $this;
    }

    /**
     * Saves a new password for the user to storage
     *
     * @param string $password
     * @param DB $db
     * @return booel
     */
    public function savePassword(string $password, DB $db) {
        $params = [
            'password'  => password_hash($password, PASSWORD_DEFAULT),
            'user_id'   => $this->getUserId()
        ];

        $db->exec("
            UPDATE users SET
                password    = :password
            WHERE user_id = :user_id",
            $params);

        return true;
    }
}