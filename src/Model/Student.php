<?php

namespace SimpleSIS\Model;

use SimpleSIS\Provider\DB;

/**
 * A student enrolled in the sis
 */
class Student {
    /** @var string[] List of available gender options */
    protected static $genders = array(
        'M' => 'Male',
        'F' => 'Female',
        'X' => 'Not Stated',
    );

    /** @var string[] List of available school years */
    protected static $school_years = array(
        'K' => 'Kindergarten',
        '1' => 'Year 1',
        '2' => 'Year 2',
        '3' => 'Year 3',
        '4' => 'Year 4',
        '5' => 'Year 5',
        '6' => 'Year 6',
    );

    /** @var int Student ID */
    protected $student_id = 0;
    /** @var string Student first name */
    protected $first_name;
    /** @var string Student surname */
    protected $surname;
    /** @var string Student gender */
    protected $gender;
    /** @var string Student school year */
    protected $school_year;

    /**
     * Constructor
     *
     * @param array $params
     */
    public function __construct($params = array()) {
        if (isset($params['student_id'])) {
            $this->setStudentId($params['student_id']);
        }
        if (isset($params['first_name'])) {
            $this->setFirstName($params['first_name']);
        }
        if (isset($params['surname'])) {
            $this->setSurname($params['surname']);
        }
        if (isset($params['gender'])) {
            $this->setGender($params['gender']);
        }
        if (isset($params['school_year'])) {
            $this->setSchoolYear($params['school_year']);
        }
    }

    /**
     * Base database query
     *
     * @return void
     */
    protected static function query() {
        return "SELECT student_id, first_name, surname, gender, school_year FROM students";
    }

    /**
     * Finds a user in the storage based off the id
     *
     * @param integer $user_id
     * @param DB $db The database connector
     * @return SimpleSis\Model\User
     */
    public static function findById(int $student_id, DB $db) {
        $record = $db->rowQuery(static::query() . " WHERE student_id = :student_id", [
            'student_id' => $student_id
        ]);

        if (!$record) {
            throw new \Exception("Object Not Found");
        }

        return new self($record);
    }

    /**
     * Looks up all students in storage
     *
     * @param DB $db The database connector
     * @return SimpleSis\Model\Student[]
     */
    public static function lookupAll(DB $db) {
        $students = array();

        $records = $db->Query(static::query() . " ORDER BY student_id");

        foreach ($records as $record) {
            $students[] = new self($record);
        }

        return $students;
    }

    /**
     * Returns the counts of students by gender
     *
     * If a gender is specified, only returns that particular count
     *
     * @param DB $db The database connector
     * @param string|null $gender
     * @return int|int[]
     */
    public static function countByGender(DB $db, $gender = null) {
        $params = [];
        $query = "SELECT gender, COUNT(*) FROM students";

        if ($gender) {
            $params['gender'] = $gender;
            $query .= " WHERE gender = :gender";
        } else {
            $query .= " GROUP BY gender";
        }

        $results = $db->indexedColumnQuery($query, $params);

        if ($gender) {
            return isset($results[0]) ? $results[0] : 0;
        } else {
            return $results;
        }
    }

    /**
     * Returns the counts of students by school year
     *
     * If a school year is specified, only returns that particular count
     *
     * @param DB $db The database connector
     * @param string|null $gender
     * @return int|int[]
     */
    public static function countBySchoolYear(DB $db, $school_year = null) {
        $params = [];
        $query = "SELECT school_year, COUNT(*) FROM students";

        if ($gender) {
            $params['school_year'] = $geschool_yearnder;
            $query .= " WHERE school_year = :school_year";
        } else {
            $query .= " GROUP BY school_year";
        }

        $results = $db->indexedColumnQuery($query, $params);

        if ($school_year) {
            return isset($results[0]) ? $results[0] : 0;
        } else {
            return $results;
        }
    }

    /**
     * Returns the student id
     *
     * @return int
     */
    public function getStudentId() {
        return $this->student_id;
    }
    /**
     * Returns the users first name
     *
     * @return string
     */
    public function getFirstName() {
        return $this->first_name;
    }
    /**
     * Returns the users surname
     *
     * @return string
     */
    public function getSurname() {
        return $this->surname;
    }
    /**
     * Returns the users full name
     *
     * @return string
     */
    public function getFullName() {
        return $this->first_name . ' ' . $this->surname;
    }
    /**
     * Returns the available genders
     *
     * @return string[]
     */
    public static function getGenders() {
        return self::$genders;
    }
    /**
     * Returns the users gender code
     *
     * @return string
     */
    public function getGenderCode() {
        return $this->gender;
    }
    /**
     * Returns the users gender
     *
     * @return string
     */
    public function getGender() {
        return self::$genders[$this->gender];
    }
    /**
     * Returns the available school years
     *
     * @return string[]
     */
    public static function getSchoolYears() {
        return self::$school_years;
    }
    /**
     * Returns the users school year code
     *
     * @return string
     */
    public function getSchoolYearCode() {
        return $this->school_year;
    }
    /**
     * Returns the users gender
     *
     * @return string
     */
    public function getSchoolYear() {
        return self::$school_years[$this->school_year];
    }

    /**
     * Sets the students id
     *
     * @param int $student_id
     * @return Student
     */
    public function setStudentId(int $student_id) {
        $this->student_id = $student_id;
        return $this;
    }
    /**
     * Sets the first name
     *
     * @param string $first_name
     * @return Student
     */
    public function setFirstName(string $first_name)  {
        $this->first_name = $first_name;
        return $this;
    }
    /**
     * Sets the surname
     *
     * @param string $surname
     * @return Student
     */
    public function setSurname(string $surname) {
        $this->surname = $surname;
        return $this;
    }
    /**
     * Sets the gender by gender code
     *
     * @param string $gender
     * @return Student
     */
    public function setGender(string $gender) {
        if (!isset(self::$genders[$gender])) {
            throw new \Exception("Unknown gender code " . $gender);
        }
        $this->gender = $gender;
        return $this;
    }
    /**
     * Sets the school year by school year code
     *
     * @param string $school_year
     * @return Student
     */
    public function setSchoolYear(string $school_year) {
        if (!isset(self::$school_years[$school_year])) {
            throw new \Exception("Unknown gender code " . $school_year);
        }
        $this->school_year = $school_year;
        return $this;
    }

    /**
     * Saves the model to storage
     *
     * @param DB $db
     * @return Student
     */
    public function save(DB $db) {
        $params = [
            'first_name'    => $this->getFirstName(),
            'surname'       => $this->getSurname(),
            'gender'        => $this->getGenderCode(),
            'school_year'   => $this->getSchoolYearCode()
        ];

        if ($this->getStudentId() == 0) {
            // New Model
            $id = $db->insert("
                INSERT INTO students (
                    first_name,
                    surname,
                    gender,
                    school_year
                ) VALUES (
                    :first_name,
                    :surname,
                    :gender,
                    :school_year
                )",
                $params);

            $this->setStudentId($id);
        } else {
            $params['student_id'] = $this->getStudentId();

            $db->exec("
                UPDATE students SET
                    first_name  = :first_name,
                    surname     = :surname,
                    gender      = :gender,
                    school_year = :school_year
                WHERE student_id = :student_id",
                $params);
        }

        return $this;
    }
}