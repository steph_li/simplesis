<?php


namespace SimpleSIS\Model;


use http\Encoding\Stream;
use SimpleSIS\Provider\DB;

class Activity
{
    /** @var int Activity ID */
    protected $activity_id = 0;
    /** @var string Activity Name */
    protected $activity_name;
    /** @var string Activity Description */
    protected $description;
    /** @var string Activity Type */
    protected $activity_type;
    /** @var string Activity Start Time */
    protected $start_time;
    /** @var string Activity End Time */
    protected $end_time;
    /** @var string Activity Location */
    protected $location;
    /** @var string Distance From School to Activity Location*/
    protected $distance_from_school;
    /** @var string Activity Organiser */
    protected $activity_organiser;

    /**
     * Constructor
     *
     * @param array $params
     */
    public function __construct($params = array()) {
        if (isset($params['activity_id'])) {
            $this->setActivityId($params['activity_id']);
        }
        if (isset($params['description'])) {
            $this->setDescription($params['description']);
        }
        if (isset($params['activity_type'])) {
            $this->setActivityType($params['activity_type']);
        }
        if (isset($params['start_time'])) {
            $this->setStartTime($params['start_time']);
        }
        if (isset($params['end_time'])) {
            $this->setEndTime($params['end_time']);
        }
        if (isset($params['location'])) {
            $this->setLocation($params['location']);
        }
        if (isset($params['activity_name'])) {
            $this->setActivityName($params['activity_name']);
        }
        if (isset($params['distance_from_school'])) {
            $this->setDistanceFromSchool($params['distance_from_school']);
        }
        if (isset($params['activity_organiser'])) {
            $this->setActivityOrganiser($params['activity_organiser']);
        }
    }

    /**
     * Looks up all activities in storage
     *
     * @param DB $db The database connector
     * @return SimpleSis\Model\Student[]
     */
    public static function lookupAll(DB $db) {
        $activities = array();

        $records = $db->Query(static::query() . " ORDER BY activity_id");

        foreach ($records as $record) {
            $activities[] = new self($record);
        }

        return $activities;
    }

    /**
     * @return string
     */
    protected static function query() {
        return "SELECT 
                    activity_id, 
                    location, 
                    description, 
                    start_time, 
                    end_time, 
                    activity_type, 
                    activity_name,
                    distance_from_school,
                    activity_organiser 
                FROM 
                    activities";
    }

    /**
     * @return string
     */
    protected static function participantQuery() {
        return "SELECT 
                    s.first_name
                FROM 
                    students s
                INNER JOIN activity_participant ap ON ap.student_id = s.student_id
                INNER JOIN activities act ON act.activity_id = ap.activity_id";
    }

    /**
     * @param int $activity_id
     * @param DB $db
     * @return Activity
     * @throws \Exception
     */
    public static function findById(int $activity_id, DB $db) {
        $record = $db->rowQuery(static::query() . " WHERE activity_id = :activity_id", [
            'activity_id' => $activity_id
        ]);

        if (!$record) {
            throw new \Exception("Object Not Found");
        }

        return new self($record);
    }

    /**
     * @param int $activity_id
     * @param DB $db
     * @return array
     * @throws \Exception
     */
    public function getStudentNameByActivityId(int $activity_id, DB $db) {
        $records = $db->query(static::participantQuery() . " WHERE act.activity_id = :activity_id", [
            'activity_id' => $activity_id
        ]);
        $result = array();

        foreach ($records as $record) {
            $result[] = $record['first_name'];
        }

        return $result;
    }

    /**
     * @param DB $db
     * @return $this
     * @throws \Exception
     */
    public function save(DB $db) {
        $params = [
            'location'             => $this->getLocation(),
            'description'          => $this->getDescription(),
            'activity_type'        => $this->getActivityType(),
            'start_time'           => $this->getStartTime(),
            'end_time'             => $this->getEndTime(),
            'activity_name'        => $this->getActivityName(),
            'distance_from_school' => $this->getDistanceFromSchool(),
            'activity_organiser'   => $this->getActivityOrganiser()
        ];

        if ($this->getActivityId() == 0) {
            // New Model
            $id = $db->insert("
                INSERT INTO activities (
                    location,
                    description,
                    activity_type,
                    start_time,
                    end_time,
                    activity_name,
                    distance_from_school,
                    activity_organiser
                ) VALUES (
                    :location,
                    :description,
                    :activity_type,
                    :start_time,
                    :end_time,
                    :activity_name,
                    :distance_from_school,
                    :activity_organiser
                )",
                $params);

            $this->setActivityId($id);
        } else {
            $params['activity_id'] = $this->getActivityId();

            $db->exec("
                UPDATE activities 
                SET
                    location             = :location,
                    description          = :description,
                    start_time           = :start_time,
                    activity_type        = :activity_type,
                    end_time             = :end_time,
                    activity_name        = :activity_name,
                    distance_from_school = :distance_from_school,
                    activity_organiser   = :activity_organiser                    
                WHERE 
                    activity_id = :activity_id",
                $params);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getActivityId() {
        return $this->activity_id;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getActivityType() {
        return $this->activity_type;
    }

    /**
     * @return string
     */
    public function getStartTime() {
        return $this->start_time;
    }

    /**
     * @return string
     */
    public function getEndTime() {
        return $this->end_time;
    }

    /**
     * @return string
     */
    public function getActivityName() {
        return $this->activity_name;
    }

    /**
     * @return string
     */
    public function getDistanceFromSchool() {
        return $this->distance_from_school;
    }

    /**
     * @return string
     */
    public function getActivityOrganiser() {
        return $this->activity_organiser;
    }

    /**
     * @param int $activity_id
     */
    public function setActivityId($activity_id)
    {
        $this->activity_id = $activity_id;
        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @param string $activity_type
     * @return $this
     */
    public function setActivityType(string $activity_type) {
        $this->activity_type = $activity_type;
        return $this;
    }

    /**
     * @param string $start_time
     * @return $this
     */
    public function setStartTime(string $start_time) {
        $this->start_time = $start_time;
        return $this;
    }

    /**
     * @param string $end_time
     * @return $this
     */
    public function setEndTime(string $end_time) {
        $this->end_time = $end_time;
        return $this;
    }

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation(string $location) {
        $this->location = $location;
        return $this;
    }

    /**
     * @param string $activity_name
     * @return $this
     */
    public function setActivityName(string $activity_name) {
        $this->activity_name = $activity_name;
        return $this;
    }

    /**
     * @param string $distance_from_school
     * @return $this
     */
    public function setDistanceFromSchool(string $distance_from_school) {
        $this->distance_from_school = $distance_from_school;
        return $this;
    }

    /**
     * @param string $activity_organiser
     * @return $this
     */
    public function setActivityOrganiser(string $activity_organiser) {
        $this->activity_organiser = $activity_organiser;
        return $this;
    }
}