<?php


namespace SimpleSIS\Model;


use SimpleSIS\Provider\DB;

class ActivityParticipant
{
    /** @var int Activity Participant ID */
    protected $activity_participant_id = 0;
    /** @var int Student ID*/
    protected $student_id;
    /** @var int Activity ID*/
    protected $activity_id;

    public function __construct($params = array()) {
        if (isset($params['$activity_participant_id'])) {
            $this->setActivityParticipantId($params['$activity_participant_id']);
        }
        if (isset($params['student'])) {
            $this->setStudent($params['student']);
        }
        if (isset($params['activity'])) {
            $this->setActivity($params['activity']);
        }
    }

    /**
     * @return int
     */
    public function getActivityParticipantId() {
        return $this->activity_participant_id;
    }

    /**
     * @return int
     */
    public function getStudentId() {
        return $this->student_id;
    }

    /**
     * @return int
     */
    public function getActivityId() {
        return $this->activity_id;
    }

    /**
     * @param int $activity_participant_id
     * @return $this
     */
    public function setActivityParticipantId(int $activity_participant_id) {
        $this->activity_participant_id = $activity_participant_id;
        return $this;
    }

    /**
     * @param int $student_id
     * @return $this
     */
    public function setStudentId(int $student_id) {
        $this->student_id = $student_id;
        return $this;
    }

    /**
     * @param int $activity_id
     * @return $this
     */
    public function setActivityId(int $activity_id) {
        $this->activity_id = $activity_id;
        return $this;
    }

    /**
     * @param DB $db
     * @return $this
     * @throws \Exception
     */
    public function save(DB $db) {
        $params = [
            'activity_id'             => $this->getActivityId(),
            'student_id'          => $this->getStudentId()
        ];

        if ($this->getActivityId() == 0) {
            // New Model
            $id = $db->insert("
                INSERT INTO activity (
                    student_id,
                    activity_id
                ) VALUES (
                    :student_id,
                    :activity_id
                )",
                $params);

            $this->setActivityParticipantId($id);
        } else {
            $params['activity_participant_id'] = $this->getActivityParticipantId();

            $db->exec("
                UPDATE 
                    activities 
                SET
                    student_id   = :student_id,
                    activity_id  = :activity_id,                 
                WHERE 
                    activity_participant_id = :activity_participant_id",
                $params);
        }

        return $this;
    }
}