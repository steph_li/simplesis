<?php

namespace SimpleSIS\Provider;

/**
 * View model for displaying html via the Smarty templating engine
 */
class View {
    /** @var Smarty The Smarty templating engine*/
    protected \Smarty $smarty;

    /** @var Session The Session */
    protected Session $session;

    /**
     * View constructor
     */
    public function __construct(Session $session) {
        $this->smarty = new \Smarty();
        $this->smarty->setTemplateDir(__DIR__ . '/../../tpl/');
        $this->smarty->setCompileDir('/tmp/smarty');
        $this->smarty->escape_html = true;

        $this->smarty->assign('session', $session);
    }

    /**
     * Assigns a value to the template
     *
     * @param string $name The name of the value
     * @param mixed $value The value to assign
     * @return void
     */
    public function assign($name, $value) {
        $this->smarty->assign($name, $value);
    }

    /**
     * Returns the rendered template
     *
     * @param string $template The template to render
     * @return void
     */
    public function render($template) {
        return $this->smarty->fetch($template);
    }
}
