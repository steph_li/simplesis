<?php

namespace SimpleSIS\Provider;

use SimpleSIS\Model\User;

/**
 * Session model for persisting state
 */
class Session {
    const SESSION_NAME = 'SimpleSIS';

    /** @var int The id of the logged in user */
    protected ?int $user_id = null;

    /** @var string The name of the logged in user */
    protected ?string $user_name = null;

    /**
     * Starts the session
     */
    public function __construct() {
        session_start();

        if (isset($_SESSION[self::SESSION_NAME])) {
            $this->load($_SESSION[self::SESSION_NAME]);
        }

        session_write_close();
    }

    /**
     * Loads the data from the session into the class
     *
     * @param array $params
     * @return void
     */
    protected function load(array $params) {
        $this->user_id = $params['user_id'];
        $this->user_name = $params['user_name'];
    }

    /**
     * Unloads the data in the session class ready to include in the session
     *
     * @return array
     */
    protected function unload() {
        $params = [
            'user_id'   => $this->getUserId(),
            'user_name' => $this->getUserName(),
        ];

        return $params;
    }

    /**
     * Whether a user has logged in
     *
     * @return boolean
     */
    public function isLoggedIn() {
        return $this->user_id > 0;
    }

    /**
     * @param [type] $user_id
     * @return int Nullable. The user id of the currently logged in user
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * @param [type] $user_name
     * @return string Nullable. The user id of the currently logged in user
     */
    public function getUserName() {
        return $this->user_name;
    }

    /**
     * Sets the user id and displayname of the session and logs them in
     *
     * If null is provided, then log out the current user
     *
     * @param SimpleSis\Model\User|null $user
     * @return SimpleSIS\Provider\Session
     */
    public function setUser(?User $user = null) {
        if ($user) {
            $this->user_id = $user->getUserId();
            $this->user_name = $user->getDisplayName();
        } else {
            $this->user_id = null;
            $this->user_name = null;
        }
        return $this;
    }

    public function save() {
        session_start();

        $_SESSION[self::SESSION_NAME] = $this->unload();

        session_write_close();
    }
}
