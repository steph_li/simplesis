<?php

namespace SimpleSIS\Provider;

use PDO;

class DB {
    protected $db_host = '';
    protected $db_user = '';
    protected $db_password = '';

    protected $pdo = null;

    public function __construct($db_host, $db_user, $db_password) {
        $this->setDBHost($db_host);
        $this->setDBUser($db_user);
        $this->setDBPassword($db_password);
    }

    public function getDBHost() {
        return $this->db_host;
    }

    public function getDBUser() {
        return $this->db_user;
    }

    public function getDBPassword() {
        return $this->db_password;
    }

    public function setDBHost(string $db_host) {
        $this->db_host = $db_host;
        return $this;
    }

    public function setDBUser(string $db_user) {
        $this->db_user = $db_user;
        return $this;
    }

    public function setDBPassword(string $db_password) {
        $this->db_password = $db_password;
        return $this;
    }

    public function connect() {
        try {
            $this->pdo = new PDO('mysql:host=' . $this->getDBHost() . ';dbname=simplesis;charset=utf8mb4', $this->getDBUser(), $this->getDBPassword());
        } catch (\PDOException $e) {
            // Catch any connection exceptions so we don't expose the password
            throw new \PDOException($e->getMessage());
        }
    }

    public function disconnect() {
        $this->pdo = null;
    }

    protected function prepare($query) {
        if ($this->pdo == null) {
            $this->connect();
        }

        return $this->pdo->prepare($query);
    }

    public function query($query, array $params = array()) {
        $statement = $this->prepare($query);
        $result = $statement->execute($params);

        if (!$result) {
            throw new \Exception(array_pop($statement->errorInfo()));
        }

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function rowQuery($query, array $params = array()) {
        $statement = $this->prepare($query);
        $result = $statement->execute($params);

        if (!$result) {
            throw new \Exception(array_pop($statement->errorInfo()));
        }

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function fieldQuery($query, array $params = array()) {
        $statement = $this->prepare($query);
        $result = $statement->execute($params);

        if (!$result) {
            throw new \Exception(array_pop($statement->errorInfo()));
        }

        $result = $statement->fetch(PDO::FETCH_NUM);

        return $result[0];
    }

    public function indexedColumnQuery($query, array $params = array()) {
        $statement = $this->prepare($query);
        $result = $statement->execute($params);

        if (!$result) {
            throw new \Exception(array_pop($statement->errorInfo()));
        }

        $records = array();
        foreach ($statement->fetchAll(PDO::FETCH_NUM) as $record) {
            $records[$record[0]] = $record[1];
        };

        return $records;
    }

    public function exec($query, array $params = array()) {
        $statement = $this->prepare($query);
        $result = $statement->execute($params);

        if (!$result) {
            throw new \Exception(array_pop($statement->errorInfo()));
        }

    }

    public function insert($query, array $params = array()) {
        $this->exec($query, $params);
        return $this->pdo->lastInsertId();
    }
}