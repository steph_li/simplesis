<?php

namespace SimpleSIS\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use SimpleSIS\Provider\Session;
use SimpleSIS\Model\Student;

/**
 * Home Functions
 */
class Home extends Base {
    /**
     * Displays the login screen
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @return Psr\Http\Message\ResponseInterface
     */
    public function get(Response $response, Session $session) {
        $this->view->assign('title', 'Home');
        $this->view->assign('menu_items', $this->menu_items);
        $this->view->assign('selected_menu_item', 'Home');

        $this->view->assign('students_by_gender', Student::countByGender($this->db));
        $this->view->assign('students_by_school_year', Student::countBySchoolYear($this->db));
        $response->getBody()->write($this->view->render('home.tpl'));
        return $response;
    }
}