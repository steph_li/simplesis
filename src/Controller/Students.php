<?php

namespace SimpleSIS\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use SimpleSIS\Provider\Session;
use SimpleSIS\Model\Student;

/**
 * Students Functions
 */
class Students extends Base {
    /**
     * Displays the student register
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @return Psr\Http\Message\ResponseInterface
     */
    public function getMany(Response $response, Session $session) {
        $this->view->assign('title', 'Students');
        $this->view->assign('menu_items', $this->menu_items);
        $this->view->assign('selected_menu_item', 'Students');

        $this->view->assign('students', Student::lookupAll($this->db));
        $response->getBody()->write($this->view->render('students.tpl'));
        return $response;
    }

    /**
     * Displays the student details
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @param string $student_id The student id to show (if the id is add then its a new student)
     * @return Psr\Http\Message\ResponseInterface
     */
    public function getOne(Response $response, Session $session, $student_id) {
        $this->view->assign('menu_items', $this->menu_items);
        $this->view->assign('selected_menu_item', 'Students');

        if ($student_id == 'add' || $student_id == 0) {
            $student = new Student();
            $this->view->assign('title', 'New Student');
        } else {
            $student = Student::findById($student_id, $this->db);
            $this->view->assign('title', $student->getFullName());
        }

        $this->view->assign('student', $student);
        $response->getBody()->write($this->view->render('student.tpl'));
        return $response;
    }


    /**
     * Displays the student details
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @param Psr\Http\Message\ServerRequestInterface $request The request sent to the browser
     * @param string $student_id The student id to show (if the id is add then its a new student)
     * @return Psr\Http\Message\ResponseInterface
     */
    public function postOne(Response $response, Request $request, Session $session, $student_id) {
        if ($student_id == 'add' || $student_id == 0) {
            $student = new Student();
        } else {
            $student = Student::findById($student_id, $this->db);
        }
        $params = (array)$request->getParsedBody();

        $student->setFirstname($params['first_name']);
        $student->setSurname($params['surname']);
        $student->setGender($params['gender']);
        $student->setSchoolYear($params['school_year']);
        $student->save($this->db);

        return $response
        ->withHeader('Location', $this->route_parser->urlFor('students'))
        ->withStatus(302);

        return $response;
    }
}