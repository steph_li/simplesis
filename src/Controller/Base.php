<?php

namespace SimpleSIS\Controller;

use SimpleSIS\Provider\View;
use SimpleSIS\Provider\DB;
use Slim\Interfaces\RouteParserInterface;
use Slim\App;

/**
 * Base Controller Model
 */
abstract class Base {
    /** @var SimpleSIS\Provider\View The view provider */
    protected View $view;
    /** @var SimpleSIS\Provider\DB The database provider */
    protected DB $db;
    /** @var Slim\Interfaces\RouteParserInterface Slims route parser interface */
    protected RouteParserInterface $route_parser;
    /** @var string[] Sidebar menu items */
    protected $menu_items = [
        'Home' => '/home',
        'Students' => '/students',
        'Activities' => '/activities'
    ];

    /**
     * Constructor
     */
    public function __construct(View $view, DB $db, App $app) {
        $this->view = $view;
        $this->db = $db;
        $this->route_parser = $app->getRouteCollector()->getRouteParser();
    }
}