<?php

namespace SimpleSIS\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Serves up JS from vendor files
 */
class JS extends Base {
    /**
     * Displays the login screen
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @param Psr\Http\Message\ServerRequestInterface $request The request sent to the browser
     * @param $name The name of the js file from the url
     *
     * @return Psr\Http\Message\ResponseInterface
     */
    public function get(Response $response, Request $request, $name) {
        $vendor_dir = __DIR__ . '/../../vendor';

        $js_sources = [
            'bootstrap.js'  => $vendor_dir . '/twbs/bootstrap/dist/js/bootstrap.bundle.min.js',
            'jquery.js'     => $vendor_dir . '/components/jquery/jquery.min.js',
        ];

        if (!isset($js_sources[$name]) || !file_exists($js_sources[$name])) {
            throw new \Slim\Exception\HttpNotFoundException($request);
        }

        $response->getBody()->write(
            file_get_contents($js_sources[$name])
        );

        return $response;
    }
}