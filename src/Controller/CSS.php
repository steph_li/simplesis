<?php

namespace SimpleSIS\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Serves up CSS from vendor files
 */
class CSS extends Base {
    /**
     * Displays the login screen
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @param Psr\Http\Message\ServerRequestInterface $request The request sent to the browser
     * @param $name The name of the css file from the url
     *
     * @return Psr\Http\Message\ResponseInterface
     */
    public function get(Response $response, Request $request, $name) {
        $vendor_dir = __DIR__ . '/../../vendor';

        $css_sources = [
            'bootstrap.css' => $vendor_dir . '/twbs/bootstrap/dist/css/bootstrap.min.css'
        ];

        if (!isset($css_sources[$name]) || !file_exists($css_sources[$name])) {
            throw new \Slim\Exception\HttpNotFoundException($request);
        }

        $response->getBody()->write(
            file_get_contents($css_sources[$name])
        );

        return $response;
    }
}