<?php

namespace SimpleSIS\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Interfaces\RouteParserInterface;

use SimpleSIS\Provider\Session;
use SimpleSIS\Model\User;

/**
 * Login Functions
 */
class Login extends Base {
    /**
     * Displays the login screen
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @param Psr\Http\Message\ServerRequestInterface $request The request sent to the browser
     * @param SimpleSIS\Provider\Session $request The current session
     * @return Psr\Http\Message\ResponseInterface
     */
    public function get(Response $response, Request $request, Session $session) {
        if ($session->isLoggedIn()) {
            $params = $request->getQueryParams();
            if (isset($params['action']) && $params['action'] == 'logout') {
                $session->setUser(null);
                $session->save();

                return $response
                    ->withHeader('Location', $this->route_parser->urlFor('login'))
                    ->withStatus(302);
            }
            return $response
                ->withHeader('Location', $this->route_parser->urlFor('home'))
                ->withStatus(302);
        }

        $response->getBody()->write($this->view->render('login.tpl'));
        return $response;
    }

    /**
     * Displays the login screen
     *
     * @param Psr\Http\Message\ResponseInterface $response The response to send to the browser
     * @param Psr\Http\Message\ServerRequestInterface $request The request sent to the browser
     * @param SimpleSIS\Provider\Session $request The current session
     * @return Psr\Http\Message\ResponseInterface
     */
    public function post(Response $response, Request $request, Session $session) {
        $params = (array)$request->getParsedBody();

        $authenticated = User::verifyPasswordByUsername($params['username'], $params['password'], $this->db);

        if ($authenticated) {
            $user = User::findByUsername($params['username'], $this->db);
            $session->setUser($user);
            $session->save();

            return $response
                ->withHeader('Location', $this->route_parser->urlFor('home'))
                ->withStatus(302);
        } else {
            $this->view->assign('login_error', 'Could not find username or password');
            $response->getBody()->write($this->view->render('login.tpl'));
        }

        return $response;
    }
}