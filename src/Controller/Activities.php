<?php


namespace SimpleSIS\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use SimpleSIS\Model\ActivityParticipant;
use SimpleSIS\Model\Student;
use Slim\Http\Response as HttpResponse;
use Psr\Http\Message\ServerRequestInterface as Request;

use SimpleSIS\Model\Activity;
use SimpleSIS\Provider\Session;

class Activities extends Base
{
    /**
     * @param Response $response
     * @param Session $session
     * @return Response
     */
    public function getAllActivities(Response $response, Session $session) {
        $this->view->assign('title', 'Activities');
        $this->view->assign('menu_items', $this->menu_items);
        $this->view->assign('selected_menu_item', 'Activities');

        $this->view->assign('activities', Activity::lookupAll($this->db));
        $response->getBody()->write($this->view->render('activities.tpl'));
        return $response;
    }

    /**
     * @param Response $response
     * @param Session $session
     * @param $activity_id
     * @return Response
     * @throws \Exception
     */
    public function getActivity(Response $response, Session $session, $activity_id) {
        $this->view->assign('menu_items', $this->menu_items);
        $this->view->assign('selected_menu_item', 'Activities');

        if ($activity_id == 'add' || $activity_id == 0) {
            $activity = new Activity();
            $this->view->assign('title', 'New Activity');
            $this->view->assign('participants', '');
        } else {
            $activity = Activity::findById($activity_id, $this->db);
            $this->view->assign('title', $activity->getActivityName());
            $participants = Activity::getStudentNameByActivityId($activity_id, $this->db);
            $participants = implode(', ', $participants);
            $this->view->assign('participants', $participants);
        }

        $this->view->assign('students', Student::lookupAll($this->db));
        $this->view->assign('activity', $activity);
        $this->view->assign('travel_distance', $activity->getDistanceFromSchool());

        $response->getBody()->write($this->view->render('activity.tpl'));

        return $response;
    }

    /**
     * @param Response $response
     * @param Request $request
     * @param Session $session
     * @param $activity_id
     * @return mixed
     * @throws \Exception
     */
    public function postActivity(Response $response, Request $request, Session $session, $activity_id) {
        if ($activity_id == 'add' || $activity_id == 0) {
            $activity = new Activity();
        } else {
            $activity = Activity::findById($activity_id, $this->db);
        }
        $params = (array)$request->getParsedBody();

        $activity->setActivityName($params['name']);
        $activity->setActivityType($params['type']);
        $activity->setDescription($params['description']);
        $activity->setStartTime($params['startTime']);
        $activity->setEndTime($params['endTime']);
        $activity->setLocation($params['location']);
        $activity->setActivityOrganiser($params['organiser']);

        $to = rawurlencode('Chatswood, AU');/* Assume the school is in Chatswood... */
        $geo_info = $this->calculateDistance(rawurlencode($params['location']), $to);
        $geo_info_array = json_decode($geo_info, true);

        if (isset($geo_info_array['resourceSets'])) {
            $itinerary_arr = $geo_info_array['resourceSets'][0]['resources'][0]['routeLegs'][0];
            $distance = $itinerary_arr['routeSubLegs'][0]['travelDistance'];
            $distance_unit = $geo_info_array['resourceSets'][0]['resources'][0]['distanceUnit'];
            $activity->setDistanceFromSchool($distance.' '.$distance_unit);
        }

        $activity->save($this->db);

        return $response
            ->withHeader('Location', $this->route_parser->urlFor('activities'))
            ->withStatus(302);

        return $response;
    }

    /**
     * @param $from
     * @param $to
     * @return bool|string
     */
    private function calculateDistance($from, $to) {
        $key = 'AjvvoCfZy2d-hf-T0XJHhf8RGh62eSHS5oGKk2im_Lz_B7nXmcaG69sfgdbAsDmI';
        $curl = curl_init();


        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://dev.virtualearth.net/REST/V1/Routes/Walking?wp.0={$from}&wp.1={$to}&output=json&key={$key}",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }
}