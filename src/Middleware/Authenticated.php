<?php

namespace SimpleSIS\Middleware;

use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * Middleware for checking if the user is authenticated
 */
class Authenticated {
    /** @var DI\Container The dependency injector container */
    protected Container $container;

    /**
     * Constructor
     *
     * @param DI\Container $container
     */
    public function __construct(Container $container) {
        $this->container = $container;
    }

    /**
     * Checks if the user is authenticated
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     * @param Psr\Http\Server\RequestHandlerInterface $handler
     * @return Psr\Http\Message\ResponseInterface
     */
    public function __invoke(Request $request, RequestHandler $handler) {
        $response = $handler->handle($request);

        if (!$this->container->get('SimpleSIS\Provider\Session')->isLoggedIn()) {
            return $response
                ->withHeader('Location', $this->container->get('Slim\App')->getRouteCollector()->getRouteParser()->urlFor('login'))
                ->withStatus(302);
        }

        return $response;
    }

}