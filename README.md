# SimpleSIS

SimpleSIS is a sample Student Information System to be used as a base for code sample exercises.

## Installation

### Docker

For docker installs, the images are already configured to run the application on a localhost. Run the following docker command from the project root to run the application.

```bash
docker-compose up -d
```

### Manual Installation

To manually run SimpleSIS you need a PHP (with PDO) server running on Apache and a MySQL server.

Copy the `apache-config.conf` to the `sites-enabled` and modify the `DocumentRoot` and `Directory` directives to point to the projects `www` directory.

Create a database in MySQL called `simplesis` and run the scripts in the `init-db` directory.

Make a copy of `config.sample.json` and name it `config.json`. Modify the values in `config.json` to match your MySQL install.

## Usage

After the sample data is loaded, the application can be accessed using the credentials `admin` \ `admin`

## Architecture

### Webserver
The application uses the [Apache FallbackResource directive](https://httpd.apache.org/docs/trunk/mod/mod_dir.html#fallbackresource) to make sure that all calls to non-static content is piped in `index.php`.

### Framework
The application leverages the [Slim Framework](https://www.slimframework.com/) to power its internals. The application is then broken down in the following sections:

#### Routes
The are two route groups in `index.php`. An unauthorised group and an authorised group. The authorised group uses [Middleware](https://www.slimframework.com/docs/v4/concepts/middleware.html) to make sure that all calls to that group are authenticated.

The groups then contain the routes which map to functions in the controllers. For example
```php
$group->get('/home', [SimpleSIS\Controller\Home::class, 'get']);
```
Adds a `GET` route to the application which is serviced by the get function in `SimpleSIS\Controller\Home`

#### Controllers
Controllers are stored in `src\Controller`. Controllers connect the models and views when servicing endpoints.

Most controllers will have a function definition that include the Request and Response to allow them to inspect and modify these objects. All of these functions should then return the modified Response object to allow the framework to server up that response to the browser.

The controllers have access to the dependency injector to including an injected class in the function definition will pass that class into the function.

If the route has any placeholders in them, the name of the placeholder can be included in the function defitinion to access that data. For example, the route
```php
$group->get('/students/{student_id}', [SimpleSIS\Controller\Students::class, 'getOne'])
```
is serviced by the following function
```php
public function getOne(Response $response, Session $session, $student_id) {
```
this allows a call to `/students/1` to populate 1 to the `$student_id` variable. You can also notice that the Session provider is also accessible in this example.

#### Middleware
Middleware is stored in `src\Middleware`. Middleware is injected by Slim before or after endpoint calls to perform a transformative action on the call. For example, the Authenticated middleware inspects the session to determine if the user is authenticated before allowing the call to proceed.

#### Models
Models are stored in `src\Model`. Models are connected to the controller and in this project are also responsible for persisting themselves in the database.

#### Providers
Providers are stored in `src\Provider`. Providers are general purpose classes used to service the application. For example, the DB provider is the interface to PDO to help persist data. Providers are injected into the rest of the app via the Dependency Injector. Because the injector has autowiring, any new providers are automatically available by defining the name of the provider in the constructor of the class that requires that provider.

#### Views
Views are stored in `tpl`. Views present the html that is then served to the user.

#### Static Files
Static files are stored in `www`. These files are served directly to the user.

### Dependency Injector
The application leverages a [PHP-DI](https://php-di.org/) Dependency Injector Container to ensure that all classes have their dependencies serviced. The PHP-DI container is [autowired](https://php-di.org/doc/autowiring.html) to automatically create and inject dependencies based on the class names.

The dependency injector also uses a [bridge to Slim](https://php-di.org/doc/frameworks/slim.html) to bring extra quality of life features to the route functions such as Request attribute injection.

### Templating Engine
The [Smarty](https://www.smarty.net/) templating engine is used to separate the views from the controllers. Smarty is injected using the View provider.

### Frontend Libraries
[Bootstrap](https://getbootstrap.com/) and [jQuery](https://jquery.com/) are provided via Composer to support rapid front end development. The CSS and JS controllers map the Composer files to endpoints served to the UI.