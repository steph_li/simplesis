CREATE TABLE `students` (
  `student_id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` TEXT,
  `surname` TEXT,
  `gender` VARCHAR(10),
  `school_year` VARCHAR(10),
  PRIMARY KEY (`student_id`)
);
