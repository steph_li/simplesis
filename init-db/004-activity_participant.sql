CREATE TABLE `activity_participant` (
  `activity_participant_id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` INT(10) unsigned NOT NULL,
  `student_id` INT(10) unsigned NOT NULL,
  PRIMARY KEY (`activity_participant_id`),
  FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`),
  FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`)
);
