CREATE TABLE `users` (
  `user_id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) DEFAULT NULL,
  `display_name` TEXT,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
);
