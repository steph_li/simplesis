CREATE TABLE `activities` (
  `activity_id` INT(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` TEXT,
  `activity_type` TEXT,
  `start_time` TEXT,
  `end_time` TEXT,
  `location` TEXT,
  `activity_name` TEXT,
  `distance_from_school` TEXT,
  `activity_organiser` TEXT,
  PRIMARY KEY (`activity_id`)
);
