/* Users */
INSERT INTO users VALUES
(null, 'admin', '$2y$10$GU.u0rLX/RfPOfcvTo0uDecb.JEavbcfISNYmSais9eOpfHAClQf.', 'Administrator');

/* Students */
INSERT INTO students VALUES
(null, 'Eadburga',  'Thorley',      'F', 'K'),
(null, 'Onóra',     'Petrescu',     'F', 'K'),
(null, 'Stela',     'Ciobanu',      'F', 'K'),
(null, 'Anantha',   'Voll',         'F', 'K'),
(null, 'Terezie',   'Houtman',      'F', '1'),
(null, 'Andria',    'Mateus',       'F', '1'),
(null, 'Linda',     'Martinović',   'F', '1'),
(null, 'Sławomira', 'Drechsler',    'F', '1'),
(null, 'Jacinto',   'Ruggeri',      'F', '1'),
(null, 'Munroe',    'Best',         'M', '1'),
(null, 'Andria',    'Mateus',       'F', '2'),
(null, 'Saundra',   'De Lang',      'F', '2'),
(null, 'Skënder',   'Both',         'M', '2'),
(null, 'Atlas',     'Negri',        'M', '2'),
(null, 'Wim',       'Ellery',       'M', '3'),
(null, 'Ham',       'Klassen',      'M', '3'),
(null, 'Jagjit',    'Forst',        'M', '3'),
(null, 'Bakhtiar',  'Buonarroti',   'M', '3'),
(null, 'Tadeu',     'Adamczyk',     'M', '3'),
(null, 'Iesus',     'Berkowicz',    'M', '3'),
(null, 'Sheraga',   'Expósito',     'M', '3'),
(null, 'Fulvius',   'McAdams',      'M', '4'),
(null, 'Sappho',    'Gang',         'F', '4'),
(null, 'Shprintze', 'Armando',      'X', '4'),
(null, 'Idalia',    'Chase',        'F', '4'),
(null, 'Gerald',    'Siegel',       'M', '4'),
(null, 'Inessa',    'Kaufer',       'F', '5'),
(null, 'Torgny',    'Lacey',        'M', '5'),
(null, 'Pamela',    'Mulligan',     'F', '5'),
(null, 'Stribog',   'Sigurdsson',   'M', '5'),
(null, 'Öztürk',    'Okamura',      'M', '6'),
(null, 'Fife',      'Dijkstra',     'M', '6'),
(null, 'Judi',      'Smith',        'F', '6'),
(null, 'Kumari',    'Vinter',       'F', '6'),
(null, 'Wynnstan',  'Vång',         'M', '6');

/* Activities */
INSERT INTO activities VALUES
(null, 'test activity 1', 'camp', '2021-05-14 10:00:00', '2021-05-14 10:00:00', 'Mascot, AU', 'Act1', '', 'admin'),
(null, 'test activity 2', 'exam', '2021-05-14 10:00:00', '2021-05-14 10:00:00', 'Lane Cove, AU', 'Act2', '', 'admin'),
(null, 'test activity 3', 'excursion', '2021-05-14 10:00:00', '2021-05-14 10:00:00', 'Epping, AU', 'Act3', '', 'admin');

INSERT INTO activity_participant VALUE
(null, 1, 1),
(null, 1, 2),
(null, 1, 3),
(null, 2, 4),
(null, 2, 5);