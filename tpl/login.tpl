<html>
    <head>
      <title>SimpleSIS: Login</title>
      <link rel="stylesheet" href="/css/bootstrap.css">
      <link rel="stylesheet" href="/css/login.css">
      <script type="text/javascript" src="/js/bootstrap.js"></script>
      <script type="text/javascript" src="/js/jquery.js"></script>
    </head>
    <body class="text-center">
      <div class="container">
        <div class="row">
          <form class="form-signin" action="" method="post">
            <h1 class="h1 mb-3 font-weight-normal">SimpleSIS</h1>
            <h2 class="h3 mb-3 font-weight-normal">Please sign in</h1>
            <label for="inputEmail" class="sr-only">Username</label>
            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
          </form>
        </div>
{if $login_error}
        <div class="row">
          <div class="alert alert-danger" role="alert">{$login_error}</div>
        </div>
{/if}
      </div>
    </body>
</html>