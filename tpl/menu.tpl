<nav class="col-md-3 col-lg-2 d-md-block bg-dark sidebar collapse">
  <div class="position-sticky pt-3">
    <a href="/home" class="dalign-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
      <span class="fs-4">SimpleSIS</span>
    </a>
    <ul class="nav nav-pills flex-column mb-auto">
{foreach $menu_items as $label => $link}
      <li class="nav-item">
        <a href="{$link}" class="nav-link{if $label == $selected} active{/if}" aria-current="page">{$label}</a>
      </li>
{/foreach}
    </ul>
  </div>
  <div class="position-fixed bottom-0">
    <div class="dropup py-2">
      <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
      <strong>{$session->getUserName()}</strong>
      </a>
      <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
      <li><a class="dropdown-item" href="/?action=logout">Log out</a></li>
      </ul>
    </div>
  </div>
</nav>
