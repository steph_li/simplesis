{include file="_header.tpl"}
<div class="row">
    <h2 class="gy-5">{$title}</h2>
    <div class="p-2">
        <a class="float-end" href="/activities">Back to Register</a>
    </div>
    <form method="post" action="">
        <div class="mb-3">
            <label for="inputActivityName" class="form-label">Activity Name</label>
            <input type="text" name="name" class="form-control" id="inputActivityName" value="{$activity->getActivityName()}">
        </div>
        <div class="mb-3">
            <label for="inputActivityOrganiser" class="form-label">Activity Organiser</label>
            <input type="text" name="organiser" class="form-control" id="inputActivityOrganiser" value="{$activity->getActivityOrganiser()}">
        </div>
        <div class="mb-3">
            <label for="inputActivityDescription" class="form-label">Activity Description</label>
            <input type="text" name="description" class="form-control" id="inputActivityDescription" value="{$activity->getDescription()}">
        </div>
        <div class="mb-3">
            <label for="inputActivityLocation" class="form-label">Activity Location</label>
            <small> (The distance of this location from school is {$travel_distance})<small>
            <input type="text" name="location" class="form-control" id="inputActivityLocation" value="{$activity->getLocation()}">
        </div>
        <div class="mb-3">
            <label for="inputActivityStartTime" class="form-label">Start Time</label>
            <input type="datetime-local" name="startTime" class="form-control" id="inputActivityStartTime" value="{$activity->getStartTime()}">
        </div>
        <div class="mb-3">
            <label for="inputActivityEndTime" class="form-label">End Time</label>
            <input type="datetime-local" name="endTime" class="form-control" id="inputActivityEndTime" value="{$activity->getEndTime()}">
        </div>
        <div class="mb-3">
            <label for="selectActivityType" class="form-label">Activity Type</label>
            <select name="type" class="form-select" id="selectActivityType">
                <option value="camp"{if $activity->getActivityType() == 'camp'} selected{/if}>Camp</option>
                <option value="excursion"{if $activity->getActivityType() == 'excursion'} selected{/if}>Excursion</option>
                <option value="exam"{if $activity->getActivityType() == 'exam'} selected{/if}>Exam</option>
                <option value="openDay"{if $activity->getActivityType() == 'openDay'} selected{/if}>Open Day</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Participants</label>
            <input type="text" name="" class="form-control" id="" value="{$participants}" disabled>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
{include file="_footer.tpl"}