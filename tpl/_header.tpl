<html>
  <head>
    <meta http-equiv="Content-Type" content="Text/HTML" />
    <meta charset="UTF-8" />
    <title>SimpleSIS: {$title}</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/app.css">
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        {include file="menu.tpl" menu_items=$menu_items selected=$selected_menu_item}
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
          <div class="container main-content d-flex flex-column">
