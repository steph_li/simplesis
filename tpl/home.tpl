{capture name="script"}
<script type="text/javascript" src="js/donut.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  var values = [{foreach $students_by_gender as $key => $value}{$value}{if !$value@last},{/if}{/foreach}];
  var colours = ["#EF476F","#118AB2","#06D6A0","#FFD166","#073B4C"];
  var labels = [{foreach $students_by_gender as $key => $value}"{$key}"{if !$value@last},{/if}{/foreach}];

  dmbChart(values, colours, labels, document.getElementById('studentsByGender'));

  var values = [{foreach $students_by_school_year as $key => $value}{$value}{if !$value@last},{/if}{/foreach}];
  var colours = ["#f94144","#f3722c","#f8961e","#f9c74f","#90be6d","#43aa8b","#577590"];
  var labels = [{foreach $students_by_school_year as $key => $value}"{$key}"{if !$value@last},{/if}{/foreach}];

  dmbChart(values, colours, labels, document.getElementById('studentsBySchoolYear'));
});
</script>
{/capture}
{include file="_header.tpl"}
<div class="row">
  <h2 class="gy-5">Demographics</h2>
  <div class="col-6">
    <div class="p-3 text-center border bg-light">
      <canvas id="studentsByGender" width="300" height="300"></canvas>
      <h4>Students By Gender</h4>
    </div>
  </div>
  <div class="col-6">
    <div class="p-3 text-center border bg-light">
      <canvas id="studentsBySchoolYear" width="300" height="300"></canvas>
      <h4>Students By School Year</h4>
    </div>
  </div>
</div>
{include file="_footer.tpl"}