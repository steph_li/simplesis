{include file="_header.tpl"}
<div class="row">
    <h2 class="gy-5">Activities Register</h2>
    <div class="p-2">
        <a class="float-end" href="activities/add">Add an activity</a>
    </div>
    <div class="p-2">
        <table class="table table-striped sortable">
            <thead>
            <tr>
                <th scope="col">Activity ID</th>
                <th scope="col">Activity Organiser</th>
                <th scope="col">Activity ype</th>
                <th scope="col">Start Time</th>
                <th scope="col">End Time</th>
                <th scope="col">Description</th>
                <th scope="col">Location</th>
                <th scope="col">Distance From School</th>
            </tr>
            </thead>
            <tbody>
            {foreach $activities as $activity}
                <tr>
                    <td scope="row"><a href="activities/{$activity->getActivityId()}">{$activity->getActivityId()}</a></td>
                    <td>{$activity->getActivityOrganiser()}</td>
                    <td>{$activity->getActivityType()}</td>
                    <td>{$activity->getStartTime()}</td>
                    <td>{$activity->getEndTime()}</td>
                    <td>{$activity->getDescription()}</td>
                    <td>{$activity->getLocation()}</td>
                    <td>{$activity->getDistanceFromSchool()}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{include file="_footer.tpl"}