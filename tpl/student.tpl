{include file="_header.tpl"}
<div class="row">
  <h2 class="gy-5">{$title}</h2>
  <div class="p-2">
    <a class="float-end" href="/students">Back to Register</a>
  </div>
  <form method="post" action="">
    <div class="mb-3">
      <label for="inputStudentFirstName" class="form-label">First Name</label>
      <input type="text" name="first_name" class="form-control" id="inputStudentFirstName" value="{$student->getFirstName()}">
    </div>
    <div class="mb-3">
      <label for="inputStudentSurname" class="form-label">Surname</label>
      <input type="text" name="surname" class="form-control" id="inputStudentSurname" value="{$student->getSurname()}">
    </div>
    <div class="mb-3">
      <label for="selectStudentGender" class="form-label">Gender</label>
      <select name="gender" class="form-select" id="selectStudentGender">
{foreach $student->getGenders() as $gender_code => $gender}
        <option value="{$gender_code}"{if $student->getGenderCode() == $gender_code} selected{/if}>{$gender}</option>
{/foreach}
      </select>
    </div>
    <div class="mb-3">
      <label for="selectStudentSchoolYear" class="form-label">Gender</label>
      <select name="school_year" class="form-select" id="selectStudentSchoolYear">
{foreach $student->getSchoolYears() as $school_year_code => $school_year}
        <option value="{$school_year_code}"{if $student->getSchoolYearCode() == $school_year_code} selected{/if}>{$school_year}</option>
{/foreach}
      </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
{include file="_footer.tpl"}