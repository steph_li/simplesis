{include file="_header.tpl"}
<div class="row">
  <h2 class="gy-5">Student Register</h2>
  <div class="p-2">
    <a class="float-end" href="students/add">Add a student</a>
  </div>
  <div class="p-2">
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Student ID</th>
          <th scope="col">First Name</th>
          <th scope="col">Surname</th>
          <th scope="col">Gender</th>
          <th scope="col">School Year</th>
        </tr>
      </thead>
      <tbody>
{foreach $students as $student}
        <tr>
          <td scope="row"><a href="students/{$student->getStudentId()}">{$student->getStudentId()}</a></td>
          <td>{$student->getFirstName()}</td>
          <td>{$student->getSurname()}</td>
          <td>{$student->getGender()}</td>
          <td>{$student->getSchoolYear()}</td>
        </tr>
{/foreach}
      </tbody>
    </table>
  </div>
</div>
{include file="_footer.tpl"}