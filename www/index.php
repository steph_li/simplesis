<?php

use DI\ContainerBuilder;
use DI\Bridge\Slim\Bridge;
use Slim\Routing\RouteCollectorProxy;

// Autoload via Composer
require __DIR__ . '/../vendor/autoload.php';

// Autowired Dependency Injector Container
$builder = new ContainerBuilder();

// Get the configuration options from config.json in the application root directory
if (!file_exists(__DIR__ . '/../config.json')) {
    throw new Exception('Could not find the config.json file');
}
$config = json_decode(file_get_contents(__DIR__ . '/../config.json'), true);

$builder->addDefinitions($config);
$builder->addDefinitions([
    'SimpleSIS\Provider\DB' => DI\Create()->constructor(DI\get('db.host'), DI\get('db.user'), DI\get('db.password'))
]);

$container = $builder->build();

// Run the Slim App via the Dependency Injector Bridge
$app = Bridge::create($container);

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

// Unauthenticated Routes
$app->group('', function (RouteCollectorProxy $group) {
    // Service Routes
    $group->get('/css/{name}', [SimpleSIS\Controller\CSS::class, 'get']);
    $group->get('/js/{name}', [SimpleSIS\Controller\JS::class, 'get']);

    // Login Routes
    $group->get('/', [SimpleSIS\Controller\Login::class, 'get'])->setName('login');
    $group->post('/', [SimpleSIS\Controller\Login::class, 'post']);
});

// Authenticated Routes
$app->group('', function (RouteCollectorProxy $group) {
    // Home Page
    $group->get('/home', [SimpleSIS\Controller\Home::class, 'get'])->setName('home');
    // Students Register
    $group->get('/students', [SimpleSIS\Controller\Students::class, 'getMany'])->setName('students');
    $group->get('/students/{student_id}', [SimpleSIS\Controller\Students::class, 'getOne'])->setName('student');
    $group->post('/students/{student_id}', [SimpleSIS\Controller\Students::class, 'postOne']);
    // Activities Section
    $group->get('/activities', [SimpleSIS\Controller\Activities::class, 'getAllActivities'])->setName('activities');
    $group->get('/activities/{activity_id}', [SimpleSIS\Controller\Activities::class, 'getActivity'])->setName('activity');
    $group->post('/activities/{activity_id}', [SimpleSIS\Controller\Activities::class, 'postActivity']);
})->add('SimpleSIS\Middleware\Authenticated');

// Run the app!
$app->run();