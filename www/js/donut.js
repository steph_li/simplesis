/* Donut Chart Javascript
https://stackoverflow.com/questions/38576678/html-css-how-to-create-a-donut-chart-like-this
*/
function dmbChart(values,colors,labels,element){
    var ctx = element.getContext("2d");
    var cx=150
    var cy=150
    var radius=100
    var arcwidth=40
    var tot=0;
    var accum=0;
    var PI=Math.PI;
    var PI2=PI*2;
    var offset=-PI/2;
    ctx.lineWidth=arcwidth;
    for(var i=0;i<values.length;i++){tot+=values[i];}
    for(var i=0;i<values.length;i++){
        ctx.beginPath();
        ctx.arc(cx,cy,radius,
            offset+PI2*(accum/tot),
            offset+PI2*((accum+values[i])/tot)
        );
        ctx.strokeStyle=colors[i];
        ctx.stroke();
        accum+=values[i];
    }
}